#!/bin/zsh
mydir=${0:a:h}

cp -f $mydir/.vimrc ~/.vimrc
cp -f $mydir/.zshrc ~/.zshrc

# git aliases
git config --global alias.nccommit 'commit -a --allow-empty-message -m ""'
git config --global alias.rpull '!git pull && git submodule foreach --recursive git pull'
