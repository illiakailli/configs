export PS1="> "
alias ls='ls -G'

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/illiak/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/illiak/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/illiak/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/illiak/Downloads/google-cloud-sdk/completion.zsh.inc'; fi
