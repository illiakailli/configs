syntax on
set breakindent
set showbreak=>>
set ignorecase
set incsearch
set hlsearch
set linebreak
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
set tabstop=2
set shiftwidth=2
set expandtab
